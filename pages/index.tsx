import React, {Fragment} from "react";

export default function IndexPage() {
  return (
    <Fragment>
      <div className="content-wrapper">
        <Header/>
        <HeroSection/>
        <WhatWeDoSection/>
        <AnalyzeNowSection/>
        <WhyChooseUsSection/>
        <OurTeamSection/>
        <OurSolutionsSection/>
        <TestimonialSection/>
        <PricingSection/>
      </div>

      <FooterSection/>

      <div className="progress-wrap">
        <svg className="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
          <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98"/>
        </svg>
      </div>
    </Fragment>
  );
}

function Header() {
  return (
    <header className="wrapper bg-soft-primary">
      <nav className="navbar center-nav transparent navbar-expand-lg navbar-light">
        <div className="container flex-lg-row flex-nowrap align-items-center">
          <div className="navbar-brand w-50">
            <a href="/">
              <img src={"/img/logo-dark.png"} srcSet={"/img/logo-dark@2x.png 2x"} alt=""/>
            </a>
          </div>

          <div className="navbar-collapse offcanvas-nav">
            <div className="offcanvas-header d-lg-none d-xl-none">
              <a href="/">
                <img src={"/img/logo-light.png"} srcSet={"/img/logo-light@2x.png 2x"} alt=""/>
              </a>

              <button type="button" className="btn-close btn-close-white offcanvas-close offcanvas-nav-close" aria-label="Close"/>
            </div>

            <ul className="navbar-nav">
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href={"#what-we-do"}>What?</a>
              </li>

              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href={"#why-choose-us"}>Why?</a>
              </li>

              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href={"#our-team"}>Our Team</a>
              </li>

              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href={"#our-solutions"}>Our Solutions</a>
              </li>

              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle" href={"#pricing"}>Pricing</a>
              </li>
            </ul>
          </div>

          <div className="navbar-other w-50 d-flex ms-auto">
            <ul className="navbar-nav flex-row align-items-center ms-auto" data-sm-skip="true">
              <li className="nav-item d-lg-none">
                <div className="navbar-hamburger">
                  <button className="hamburger animate plain" data-toggle="offcanvas-nav">
                    <span/>
                  </button>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
  );
}

function HeroSection() {
  return (
    <section className="wrapper bg-gradient-primary">
      <div className="container pt-10 pt-md-14 pb-8 text-center">
        <div className="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
          <div className="col-lg-7">
            <figure>
              <img className="w-auto" src={"/img/concept/concept2.png"} srcSet={"/img/concept/concept2@2x.png 2x"} alt=""/>
            </figure>
          </div>

          <div className="col-md-10 offset-md-1 offset-lg-0 col-lg-5 text-center text-lg-start">
            <h1 className="display-1 mb-5 mx-md-n5 mx-lg-0">
              Grow Your Business with Our Solutions.
            </h1>

            <p className="lead fs-lg mb-7">
              We help our clients to increase their website traffic, rankings and
              visibility in search results.
            </p>

            <span>
                <a className="btn btn-primary rounded-pill me-2">Try It For Free</a>
              </span>
          </div>
        </div>
      </div>
    </section>
  );
}

function WhatWeDoSection() {
  return (
    <section className="wrapper bg-light" id="what-we-do">
      <div className="container pt-14 pt-md-16">
        <div className="row text-center">
          <div className="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
            <h2 className="fs-16 text-uppercase text-muted mb-3">What We Do?</h2>
            <h3 className="display-4 mb-10 px-xl-10">The service we offer is specifically designed to meet your
              needs.</h3>
          </div>
        </div>

        <div className="position-relative">
          <div className="shape rounded-circle bg-soft-blue rellax w-16 h-16" data-rellax-speed={1} style={{
            bottom: '-0.5rem',
            right: '-2.2rem',
            zIndex: 0
          }}/>
          <div className="shape bg-dot primary rellax w-16 h-17" data-rellax-speed={1} style={{
            top: '-0.5rem',
            left: '-2.5rem',
            zIndex: 0
          }}/>
          <div className="row gx-md-5 gy-5 text-center">
            <div className="col-md-6 col-xl-3">
              <div className="card shadow-lg">
                <div className="card-body">
                  <img src={"/img/icons/search-1.svg"} className="svg-inject icon-svg icon-svg-md text-yellow mb-3" alt=""/>
                  <h4>SEO Services</h4>
                  <p className="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at
                    eget metus cras justo.</p>
                  <a href="#" className="more hover link-yellow">Learn More</a>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-xl-3">
              <div className="card shadow-lg">
                <div className="card-body">
                  <img src={"/img/icons/browser.svg"} className="svg-inject icon-svg icon-svg-md text-red mb-3" alt=""/>
                  <h4>Web Design</h4>
                  <p className="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at
                    eget metus cras justo.</p>
                  <a href="#" className="more hover link-red">Learn More</a>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-xl-3">
              <div className="card shadow-lg">
                <div className="card-body">
                  <img src={"/img/icons/chat-2.svg"} className="svg-inject icon-svg icon-svg-md text-green mb-3" alt=""/>
                  <h4>Social Engagement</h4>
                  <p className="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at
                    eget metus cras justo.</p>
                  <a href="#" className="more hover link-green">Learn More</a>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-xl-3">
              <div className="card shadow-lg">
                <div className="card-body">
                  <img src={"/img/icons/megaphone.svg"} className="svg-inject icon-svg icon-svg-md text-blue mb-3" alt=""/>
                  <h4>Content Marketing</h4>
                  <p className="mb-2">Nulla vitae elit libero, a pharetra augue. Donec id elit non mi porta gravida at
                    eget metus cras justo.</p>
                  <a href="#" className="more hover link-blue">Learn More</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function AnalyzeNowSection() {
  return (
    <section className="wrapper bg-gradient-reverse-primary">
      <div className="container py-16 py-md-18">
        <div className="row gx-lg-8 gx-xl-12 gy-10 mb-8 align-items-center">
          <div className="col-lg-7 order-lg-2">
            <figure>
              <img className="w-auto" src={"/img/concept/concept3.png"} srcSet={"/img/concept/concept3@2x.png 2x"} alt=""/>
            </figure>
          </div>

          <div className="col-lg-5">
            <h2 className="fs-16 text-uppercase text-muted mb-3">Analyze Now</h2>

            <h3 className="display-4 mb-5">
              Wonder how much faster your website can go? Easily check your SEO Score now.
            </h3>

            <p className="mb-7">
              Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna, vel
              scelerisque nisl consectetur et.
            </p>

            <div className="row">
              <div className="col-lg-9">
                <form action="#">
                  <div className="form-label-group input-group">
                    <input type="url" className="form-control" placeholder="Enter Website URL" id="seo-check"/>
                    <label htmlFor="seo-check">Enter Website URL</label>
                    <button className="btn btn-primary" type="button">Check</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function WhyChooseUsSection() {
  return (
    <section className="wrapper bg-light angled upper-start lower-start" id="why-choose-us">
      <div className="container py-14 pt-md-17 pb-md-15">
        <div className="row gx-md-8 gx-xl-12 gy-10 mb-14 mb-md-18 align-items-center">
          <div className="col-lg-6 order-lg-2">
            <div className="card shadow-lg me-lg-6">
              <div className="card-body p-6">
                <div className="d-flex flex-row">
                  <div>
                    <span className="icon btn btn-circle btn-lg btn-soft-primary disabled me-4"><span className="number">01</span></span>
                  </div>

                  <div>
                    <h4 className="mb-1">Collect Ideas</h4>
                    <p className="mb-0">Nulla vitae elit libero pharetra augue dapibus.</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="card shadow-lg ms-lg-13 mt-6">
              <div className="card-body p-6">
                <div className="d-flex flex-row">
                  <div>
                      <span className="icon btn btn-circle btn-lg btn-soft-primary disabled me-4">
                        <span className="number">02</span>
                      </span>
                  </div>

                  <div>
                    <h4 className="mb-1">Data Analysis</h4>
                    <p className="mb-0">Vivamus sagittis lacus vel augue laoreet.</p>
                  </div>
                </div>
              </div>
            </div>

            <div className="card shadow-lg mx-lg-6 mt-6">
              <div className="card-body p-6">
                <div className="d-flex flex-row">
                  <div>
                    <span className="icon btn btn-circle btn-lg btn-soft-primary disabled me-4"><span className="number">03</span></span>
                  </div>
                  <div>
                    <h4 className="mb-1">Finalize Product</h4>
                    <p className="mb-0">Cras mattis consectetur purus sit amet.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-6">
            <h2 className="fs-16 text-uppercase text-muted mb-3">
              Our Strategy
            </h2>

            <h3 className="display-4 mb-5">
              Here are 3 working steps to organize our business projects.
            </h3>

            <p>
              Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Etiam porta sem
              malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Nullam quis risus
              eget urna mollis.
            </p>

            <p className="mb-6">
              Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta
              felis euismod semper. Aenean lacinia bibendum nulla sed consectetur.
            </p>

            <a href="#" className="btn btn-primary rounded-pill mb-0">
              Learn More
            </a>
          </div>
        </div>

        <div className="row gx-lg-8 gx-xl-12 gy-10 mb-lg-22 mb-xl-24 align-items-center">
          <div className="col-lg-7">
            <figure>
              <img className="w-auto" src={"/img/concept/concept6.png"} srcSet={"/img/concept/concept6@2x.png 2x"} alt=""/>
            </figure>
          </div>

          <div className="col-lg-5">
            <h2 className="fs-16 text-uppercase text-muted mb-3">
              Why Choose Us?
            </h2>

            <h3 className="display-4 mb-7">
              We bring solutions to make life easier for our clients.
            </h3>

            <div className="accordion accordion-wrapper" id="accordionExample">
              <div className="card plain accordion-item">
                <div className="card-header" id="headingOne">
                  <button className="accordion-button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"> Professional
                    Design
                  </button>
                </div>

                <div id="collapseOne" className="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                  <div className="card-body">
                    <p>
                      Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa
                      justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus
                      magna, vel.
                    </p>
                  </div>
                </div>
              </div>

              <div className="card plain accordion-item">
                <div className="card-header" id="headingTwo">
                  <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Top-Notch
                    Support
                  </button>
                </div>

                <div id="collapseTwo" className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                  <div className="card-body">
                    <p>
                      Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa
                      justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus
                      magna, vel.
                    </p>
                  </div>
                </div>
              </div>

              <div className="card plain accordion-item">
                <div className="card-header" id="headingThree">
                  <button className="collapsed" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Header
                    and Slider Options
                  </button>
                </div>

                <div id="collapseThree" className="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                  <div className="card-body">
                    <p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa
                      justo sit amet risus. Cras mattis consectetur purus sit amet fermentum. Praesent commodo cursus
                      magna, vel.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function OurTeamSection() {
  return (
    <section className="wrapper bg-gradient-primary" id="our-team">
      <div className="container py-14 pt-md-16 pb-md-18">
        <div className="position-relative mt-lg-n23 mt-xl-n25">
          <div className="row text-center">
            <div className="col-lg-6 mx-auto">
              <h2 className="fs-16 text-uppercase text-muted mb-3">Our Team</h2>
              <h3 className="display-4 mb-10">Think unique and be innovative. Make a difference with Sandbox.</h3>
            </div>
          </div>
          <div className="position-relative">
            <div className="shape bg-dot blue rellax w-16 h-17" data-rellax-speed={1} style={{
              bottom: '0.5rem',
              right: '-1.7rem',
              zIndex: 0
            }}/>
            <div className="shape rounded-circle bg-line red rellax w-16 h-16" data-rellax-speed={1} style={{
              top: '0.5rem',
              left: '-1.7rem',
              zIndex: 0
            }}/>
            <div className="row grid-view gy-6 gy-xl-0">
              <div className="col-md-6 col-xl-3">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <img className="rounded-circle w-15 mb-4" src={"/img/avatars/te1.jpg"} srcSet={"/img/avatars/te1@2x.jpg 2x"} alt=""/>
                    <h4 className="mb-1">Coriss Ambady</h4>
                    <div className="meta mb-2">Financial Analyst</div>
                    <p className="mb-2">Fermentum massa justo sit amet risus morbi leo.</p>
                    <nav className="nav social mb-0">
                      <a href="#"><i className="uil uil-twitter"/></a>
                      <a href="#"><i className="uil uil-facebook-f"/></a>
                      <a href="#"><i className="uil uil-dribbble"/></a>
                    </nav>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-xl-3">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <img className="rounded-circle w-15 mb-4" src={"/img/avatars/te2.jpg"} srcSet={"/img/avatars/te2@2x.jpg 2x"} alt=""/>
                    <h4 className="mb-1">Cory Zamora</h4>
                    <div className="meta mb-2">Marketing Specialist</div>
                    <p className="mb-2">Fermentum massa justo sit amet risus morbi leo.</p>
                    <nav className="nav social mb-0">
                      <a href="#"><i className="uil uil-twitter"/></a>
                      <a href="#"><i className="uil uil-facebook-f"/></a>
                      <a href="#"><i className="uil uil-dribbble"/></a>
                    </nav>
                  </div>
                </div>
              </div>
              <div className="col-md-6 col-xl-3">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <img className="rounded-circle w-15 mb-4" src={"/img/avatars/te3.jpg"} srcSet={"/img/avatars/te3@2x.jpg 2x"} alt=""/>
                    <h4 className="mb-1">Nikolas Brooten</h4>
                    <div className="meta mb-2">Sales Manager</div>
                    <p className="mb-2">Fermentum massa justo sit amet risus morbi leo.</p>
                    <nav className="nav social mb-0">
                      <a href="#"><i className="uil uil-twitter"/></a>
                      <a href="#"><i className="uil uil-facebook-f"/></a>
                      <a href="#"><i className="uil uil-dribbble"/></a>
                    </nav>
                  </div>
                </div>
              </div>

              <div className="col-md-6 col-xl-3">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <img className="rounded-circle w-15 mb-4" src={"/img/avatars/te4.jpg"} srcSet={"/img/avatars/te4@2x.jpg 2x"} alt=""/>
                    <h4 className="mb-1">Jackie Sanders</h4>
                    <div className="meta mb-2">Investment Planner</div>
                    <p className="mb-2">Fermentum massa justo sit amet risus morbi leo.</p>
                    <nav className="nav social mb-0">
                      <a href="#"><i className="uil uil-twitter"/></a>
                      <a href="#"><i className="uil uil-facebook-f"/></a>
                      <a href="#"><i className="uil uil-dribbble"/></a>
                    </nav>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function OurSolutionsSection() {
  return (
    <section className="wrapper bg-light" id="our-solutions">
      <div className="container">
        <div className="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
          <div className="col-lg-7 order-lg-2">
            <figure>
              <img className="w-auto" src={"/img/concept/concept8.png"} srcSet={"/img/concept/concept8@2x.png 2x"} alt=""/>
            </figure>
          </div>

          <div className="col-lg-5">
            <h2 className="fs-16 text-uppercase text-muted mb-3">
              Our Solutions
            </h2>

            <h3 className="display-4 mb-5">
              We make your spending stress-free for you to have the perfect control.
            </h3>

            <p className="mb-6">
              Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
              Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus.
            </p>

            <div className="row gy-3">
              <div className="col-xl-6">
                <ul className="icon-list bullet-bg bullet-soft-primary mb-0">
                  <li>
                    <span> <i className="uil uil-check"/></span>
                    <span>Aenean quam ornare. Curabitur blandit. </span>
                  </li>

                  <li className="mt-3">
                    <span><i className="uil uil-check"/></span>
                    <span>Nullam quis risus eget urna mollis ornare.</span>
                  </li>
                </ul>
              </div>

              <div className="col-xl-6">
                <ul className="icon-list bullet-bg bullet-soft-primary mb-0">
                  <li>
                    <span><i className="uil uil-check"/></span>
                    <span>Etiam porta euismod malesuada mollis.</span>
                  </li>

                  <li className="mt-3">
                    <span><i className="uil uil-check"/></span>
                    <span>Vivamus sagittis lacus vel augue rutrum.</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function TestimonialSection() {
  return (
    <section className="wrapper bg-gradient-reverse-primary" id="testimonial">
      <div className="container py-14 py-md-18">
        <div className="row gx-lg-8 gx-xl-12 gy-10 align-items-center">
          <div className="col-lg-7">
            <div className="row gx-md-5 gy-5">
              <div className="col-md-6 col-xl-5 align-self-end">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <blockquote className="icon mb-0">
                      <p>“Cum sociis natoque penatibus et magnis dis parturient montes.”</p>
                      <div className="blockquote-details">
                        <div className="info p-0">
                          <h5 className="mb-1">Coriss Ambady</h5>
                          <p className="mb-0">Financial Analyst</p>
                        </div>
                      </div>
                    </blockquote>
                  </div>
                </div>
              </div>

              <div className="col-md-6 align-self-end">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <blockquote className="icon mb-0">
                      <p>
                        “Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Vestibulum id ligula
                        porta felis euismod.”
                      </p>
                      <div className="blockquote-details">
                        <div className="info p-0">
                          <h5 className="mb-1">Cory Zamora</h5>
                          <p className="mb-0">Marketing Specialist</p>
                        </div>
                      </div>
                    </blockquote>
                  </div>
                </div>
              </div>

              <div className="col-md-6 col-xl-5 offset-xl-1">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <blockquote className="icon mb-0">
                      <p>
                        “Donec id elit non porta gravida at eget metus. Duis mollis est luctus commodo nisi erat.”
                      </p>
                      <div className="blockquote-details">
                        <div className="info p-0">
                          <h5 className="mb-1">Barclay Widerski</h5>
                          <p className="mb-0">Sales Specialist</p>
                        </div>
                      </div>
                    </blockquote>
                  </div>
                </div>
              </div>

              <div className="col-md-6 align-self-start">
                <div className="card shadow-lg">
                  <div className="card-body">
                    <blockquote className="icon mb-0">
                      <p>“Nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean eu leo pellentesque.”</p>
                      <div className="blockquote-details">
                        <div className="info p-0">
                          <h5 className="mb-1">Jackie Sanders</h5>
                          <p className="mb-0">Investment Planner</p>
                        </div>
                      </div>
                    </blockquote>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="col-lg-5">
            <h2 className="fs-16 text-uppercase text-muted mb-3 mt-lg-n6">Our Community</h2>
            <h3 className="display-4 mb-5">Don't take our word for it. See what customers are saying about us.</h3>
            <p>
              Aenean lacinia bibendum nulla sed consectetur. Praesent commodo cursus magna, vel scelerisque nisl
              consectetur et. Nulla vitae elit libero, a pharetra augue. Maecenas faucibus mollis interdum. Vestibulum
              id ligula porta felis euismod.
            </p>
            <a href="#" className="btn btn-primary rounded-pill mt-3">All Testimonials</a>
          </div>
        </div>
      </div>
    </section>
  );
}

function PricingSection() {
  return (
    <section className="wrapper bg-light angled upper-end lower-end" id="pricing">
      <div className="container py-14 pt-md-14 pb-md-18">
        <div className="row gy-6 mb-14 mb-md-18">
          <div className="col-lg-4">
            <h2 className="fs-16 text-uppercase text-muted mt-lg-18 mb-3">
              Our Pricing
            </h2>

            <h3 className="display-4 mb-3">
              We offer great and premium prices.
            </h3>

            <p>
              Enjoy a <a href="#" className="hover">free 30-day trial</a> and experience the full service.
              No credit card required!
            </p>

            <a href="#" className="btn btn-primary rounded-pill mt-2">See All Prices</a>
          </div>

          <div className="col-lg-7 offset-lg-1 pricing-wrapper">
            <div className="pricing-switcher-wrapper switcher justify-content-start justify-content-lg-end">
              <p className="mb-0 pe-3">Monthly</p>

              <div className="pricing-switchers">
                <div className="pricing-switcher pricing-switcher-active"></div>
                <div className="pricing-switcher"></div>
                <div className="switcher-button bg-primary"></div>
              </div>

              <p className="mb-0 ps-3">
                Yearly <span className="text-red">(Save 30%)</span>
              </p>
            </div>

            <div className="row gy-6 position-relative mt-5">
              <div className="shape bg-dot red rellax w-16 h-18" data-rellax-speed={1} style={{
                bottom: '-0.5rem',
                right: '-1.6rem'
              }}/>

              <div className="col-md-6">
                <div className="pricing card shadow-lg">
                  <div className="card-body pb-12">
                    <div className="prices text-dark">
                      <div className="price price-show">
                        <span className="price-currency">$</span><span className="price-value">19</span>
                        <span className="price-duration">month</span>
                      </div>

                      <div className="price price-hide price-hidden">
                        <span className="price-currency">$</span>
                        <span className="price-value">199</span>
                        <span className="price-duration">year</span>
                      </div>
                    </div>

                    <h4 className="card-title mt-2">Premium Plan</h4>

                    <ul className="icon-list bullet-soft-primary mt-8 mb-9">
                      <li><i className="uil uil-check fs-21"/><span><strong>5</strong> Projects </span></li>
                      <li><i className="uil uil-check fs-21"/><span><strong>100K</strong> API Access </span></li>
                      <li><i className="uil uil-check fs-21"/><span><strong>200MB</strong> Storage </span></li>
                      <li><i className="uil uil-check fs-21"/><span> Weekly <strong>Reports</strong></span></li>
                      <li><i className="uil uil-check fs-21"/><span> 7/24 <strong>Support</strong></span></li>
                    </ul>

                    <a href="#" className="btn btn-primary rounded-pill">Choose Plan</a>
                  </div>
                </div>
              </div>

              <div className="col-md-6 popular">
                <div className="pricing card shadow-lg">
                  <div className="card-body pb-12">
                    <div className="prices text-dark">
                      <div className="price price-show">
                        <span className="price-currency">$</span>
                        <span className="price-value">49</span>
                        <span className="price-duration">month</span>
                      </div>
                      <div className="price price-hide price-hidden">
                        <span className="price-currency">$</span>
                        <span className="price-value">499</span>
                        <span className="price-duration">year</span>
                      </div>
                    </div>

                    <h4 className="card-title mt-2">Corporate Plan</h4>
                    <ul className="icon-list bullet-soft-primary mt-8 mb-9">
                      <li><i className="uil uil-check fs-21"/><span><strong>20</strong> Projects </span></li>
                      <li><i className="uil uil-check fs-21"/><span><strong>300K</strong> API Access </span></li>
                      <li><i className="uil uil-check fs-21"/><span><strong>500MB</strong> Storage </span></li>
                      <li><i className="uil uil-check fs-21"/><span> Weekly <strong>Reports</strong></span></li>
                      <li><i className="uil uil-check fs-21"/><span> 7/24 <strong>Support</strong></span></li>
                    </ul>
                    <a href="#" className="btn btn-primary rounded-pill">Choose Plan</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row gx-lg-8 gx-xl-12 gy-10 mb-10 mb-md-14 align-items-center">
          <div className="col-lg-7">
            <figure>
              <img className="w-auto" src={"/img/concept/concept5.png"} srcSet={"/img/concept/concept5@2x.png 2x"} alt=""/>
            </figure>
          </div>

          <div className="col-lg-5">
            <h2 className="fs-16 text-uppercase text-muted mb-3">
              Let’s Talk
            </h2>

            <h3 className="display-4 mb-3">
              Let's make something great together. We are trusted by over 5000+ clients.
            </h3>

            <p>
              Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis
              interdum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa
              justo sit amet risus.
            </p>

            <a href="#" className="btn btn-primary rounded-pill mt-2">Join Us</a>
          </div>
        </div>

        <div className="px-lg-5">
          <div className="row gx-0 gx-md-8 gx-xl-12 gy-8 align-items-center">
            <div className="col-4 col-md-2">
              <figure className="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4">
                <img src={"/img/brands/c1.png"} alt=""/>
              </figure>
            </div>

            <div className="col-4 col-md-2">
              <figure className="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4">
                <img src={"/img/brands/c2.png"} alt=""/>
              </figure>
            </div>

            <div className="col-4 col-md-2">
              <figure className="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4">
                <img src={"/img/brands/c3.png"} alt=""/>
              </figure>
            </div>

            <div className="col-4 col-md-2">
              <figure className="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4">
                <img src={"/img/brands/c4.png"} alt=""/>
              </figure>
            </div>

            <div className="col-4 col-md-2">
              <figure className="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4">
                <img src={"/img/brands/c5.png"} alt=""/>
              </figure>
            </div>

            <div className="col-4 col-md-2">
              <figure className="px-5 px-md-0 px-lg-2 px-xl-3 px-xxl-4">
                <img src={"/img/brands/c6.png"} alt=""/>
              </figure>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

function FooterSection() {
  return (
    <footer className="bg-navy text-inverse">
      <div className="container pt-15 pt-md-17 pb-13 pb-md-15">
        <div className="d-lg-flex flex-row align-items-lg-center">
          <h3 className="display-4 mb-6 mb-lg-0 pe-lg-20 pe-xl-22 pe-xxl-25 text-white">
            Join our community by using our services and grow your business.
          </h3>

          <a href="#" className="btn btn-primary rounded-pill mb-0 text-nowrap">Try It For Free</a>
        </div>

        <hr className="mt-11 mb-12"/>

        <div className="row gy-6 gy-lg-0">
          <div className="col-md-4 col-lg-3">
            <div className="widget">
              <img className="mb-4" src={"/img/logo-light.png"} srcSet={"/img/logo-light@2x.png 2x"} alt=""/>

              <p className="mb-4">© 2021 Sandbox. <br className="d-none d-lg-block"/>All rights reserved.</p>

              <nav className="nav social social-white">
                <a href="#"><i className="uil uil-twitter"/></a>
                <a href="#"><i className="uil uil-facebook-f"/></a>
                <a href="#"><i className="uil uil-dribbble"/></a>
                <a href="#"><i className="uil uil-instagram"/></a>
                <a href="#"><i className="uil uil-youtube"/></a>
              </nav>
            </div>
          </div>

          <div className="col-md-4 col-lg-3">
            <div className="widget">
              <h4 className="widget-title text-white mb-3">Get in Touch</h4>
              <address className="pe-xl-15 pe-xxl-17">Moonshine St. 14/05 Light City, London, United Kingdom</address>
              <a href="mailto:#">info@email.com</a><br/> +00 (123) 456 78 90
            </div>
          </div>

          <div className="col-md-4 col-lg-3">
            <div className="widget">
              <h4 className="widget-title text-white mb-3">Learn More</h4>
              <ul className="list-unstyled mb-0">
                <li><a href="#">About Us</a></li>
                <li><a href="#">Our Story</a></li>
                <li><a href="#">Projects</a></li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Privacy Policy</a></li>
              </ul>
            </div>
          </div>

          <div className="col-md-12 col-lg-3">
            <div className="widget">
              <h4 className="widget-title text-white mb-3">Our Newsletter</h4>
              <p className="mb-5">Subscribe to our newsletter to get our news & deals delivered to you.</p>
              <div className="newsletter-wrapper">
                <div id="mc_embed_signup2">
                  <form action="https://elemisfreebies.us20.list-manage.com/subscribe/post?u=aa4947f70a475ce162057838d&amp;id=b49ef47a9a" method="post" id="mc-embedded-subscribe-form2" name="mc-embedded-subscribe-form" className="validate dark-fields" target="_blank" noValidate>
                    <div id="mc_embed_signup_scroll2">
                      <div className="mc-field-group input-group form-label-group">
                        <input type="email" defaultValue="" name="EMAIL" className="required email form-control" placeholder="Email Address" id="mce-EMAIL2"/>
                        <label htmlFor="mce-EMAIL2">Email Address</label>
                        <input type="submit" defaultValue="Join" name="subscribe" id="mc-embedded-subscribe2" className="btn btn-primary"/>
                      </div>

                      <div id="mce-responses2" className="clear">
                        <div className="response" id="mce-error-response2" style={{display: "none"}}/>
                        <div className="response" id="mce-success-response2" style={{display: "none"}}/>
                      </div>

                      <div style={{position: 'absolute', left: '-5000px'}} aria-hidden="true">
                        <input type="text" name="b_ddc180777a163e0f9f66ee014_4b1bcfa0bc" tabIndex={-1} defaultValue=""/>
                      </div>

                      <div className="clear"/>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
